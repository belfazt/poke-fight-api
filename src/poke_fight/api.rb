# frozen_string_literal: true

require 'grape'
require 'grape_logging'
require 'grape-swagger'

require_relative 'api_v1/common_moves'
require_relative 'api_v1/battle'

require_relative 'exceptions'

module PokeFight
  ##
  # Main API instatiation
  class API < Grape::API
    logger.formatter = GrapeLogging::Formatters::Default.new
    use GrapeLogging::Middleware::RequestLogger, {
      logger: logger,
      level: LOGGER.level
    }
    format :json
    prefix :api

    rescue_from PokeFight::CustomError do |e|
      LOGGER.error(e)
      error!({ error: e.message }, e.http_code)
    end

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!(e, 400)
    end

    rescue_from :all do |e|
      LOGGER.error(e)
      error!({ error: 'Something went wrong' }, 500)
    end

    mount APIv1::CommonMoves
    mount APIv1::Battle

    desc 'Returns the status of the API'
    get '/status' do
      { status: :ok }
    end

    add_swagger_documentation \
      mount_path: '/docs/swagger.json', \
      host: 'pokedex.latortuga.ninja', \
      info: {
        title: 'PokeFight API',
        description: 'Allows you to fetch how different pokemons would battle against each other'
      }
  end
end
