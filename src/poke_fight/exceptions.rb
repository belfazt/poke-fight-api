# frozen_string_literal: true

require_relative 'util'

module PokeFight
  ##
  # :nodoc: #
  class CustomError < StandardError
    attr_reader :message, :http_code
    HTTP_CODE = 500
    def initialize(_http_code = 500, message = 'Something went wrong')
      @message = message
      @http_code = HTTP_CODE
    end
  end

  ##
  # :nodoc: #
  class BadRequestError < CustomError
    HTTP_CODE = 400
    def initialize(message = 'Bad request')
      @message = message
      @http_code = HTTP_CODE
    end
  end

  ##
  # :nodoc: #
  class UnsupportedLanguageError < BadRequestError
    HTTP_CODE = 412
    def initialize(language_key)
      @message = "Could not find localization for the #{Util.languages[language_key]} language"
      @http_code = HTTP_CODE
    end
  end

  ##
  # :nodoc: #
  class UndiscoveredPokemon < BadRequestError
    HTTP_CODE = 404
    def initialize(id_or_name)
      @message = "Could not find a pokemon with an id or name '#{id_or_name}', maybe it hasn't been discovered!"
      @http_code = HTTP_CODE
    end
  end

  ##
  # :nodoc: #
  class InternalServerError < CustomError; end
end
