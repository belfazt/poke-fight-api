# frozen_string_literal: true

require_relative 'base'

require_relative '../repository/pokemon'

module PokeFight
  module CachedRepository
    ##
    # CachedRepository to help with moves
    class Pokemon < Base
      CACHE_KEY_PREFIX = 'pokemon_'

      REPOSITORY_CLASS = PokeFight::Repository::Pokemon
    end
  end
end
