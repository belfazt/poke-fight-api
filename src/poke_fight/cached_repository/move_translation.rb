# frozen_string_literal: true

require_relative '../connections'

require_relative 'base'
require_relative 'move'

require 'etc'

module PokeFight
  module CachedRepository
    ##
    # CachedRepository to help with translations
    class MoveTranslation < Base
      CACHE_KEY_PREFIX = 'move_translation_'

      class << self
        def init
          Parallel.each(Move.fetch_all, in_threads: Etc.nprocessors) { |move| find(move.name) }
        end

        def find(name)
          found = Connections.memcached.get(cache_key(:find, name))

          if found
            LOGGER.debug("Found move translation for '#{found}' in cache")
            return found
          end

          move = Move.find(name)

          lookup_hash(move).tap { |r| Connections.memcached.set(cache_key(:find, move.name), r) }
        end

        private

        def lookup_hash(move)
          { move: move, names: move.names.each_with_object({}) { |n, h| h[n.language.name] = n } }
        end
      end
    end
  end
end
