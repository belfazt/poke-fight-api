# frozen_string_literal: true

require_relative 'base'

require 'parallel'

require_relative '../repository/move'

require 'etc'

module PokeFight
  module CachedRepository
    ##
    # CachedRepository to help with moves
    class Move < Base
      CACHE_KEY_PREFIX = 'move_'

      REPOSITORY_CLASS = PokeFight::Repository::Move

      class << self
        def init
          LOGGER.info('Initializing moves cache')
          Parallel.each(fetch_all, in_threads: Etc.nprocessors) { |move| find(move.name) }
        end

        def find(name)
          found = Connections.memcached.get(cache_key(:find, name))

          if found
            LOGGER.debug("Found move '#{found.name}' in cache")
            return found
          end

          LOGGER.debug("Couldn't find move '#{name}' to cache")

          PokeFight::Repository::Move.find(name).tap do |m|
            LOGGER.debug("Adding move '#{m.name}' to cache")
            Connections.memcached.set(cache_key(:find, m.name), m)
          end
        end

        def fetch_all
          found = Connections.memcached.get(cache_key(:fetch_all))

          if found
            LOGGER.debug('Found all moves in cache')
            return found
          end

          PokeFight::Repository::Move.fetch_all.tap do |moves|
            LOGGER.debug("Adding results of 'fetch_all' to cache")
            Connections.memcached.set(cache_key(:fetch_all), moves)
          end
        end
      end
    end
  end
end
