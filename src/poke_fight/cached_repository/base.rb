# frozen_string_literal: true

require 'parallel'

require_relative '../connections'

require 'etc'

module PokeFight
  module CachedRepository
    ##
    # Base CachedRepository class
    class Base
      class << self
        def find(arg)
          found = Connections.memcached.get(cache_key(:find, arg))

          if found
            LOGGER.debug("Found '#{self::REPOSITORY_CLASS}-#{arg}' in cache")
            return found
          end

          LOGGER.debug("Couldn't find '#{self::REPOSITORY_CLASS}-#{arg}' to cache")

          self::REPOSITORY_CLASS.find(arg).tap do |m|
            LOGGER.debug("Adding '#{self::REPOSITORY_CLASS}-#{arg}' to cache")
            Connections.memcached.set(cache_key(:find, arg), m)
          end
        end

        def list(ids_or_names)
          Parallel.map(ids_or_names, in_threads: Etc.nprocessors) { |e| find(e) }
        end

        private

        def cache_key(method_name, params = '--no_params--')
          ['repository_', self::CACHE_KEY_PREFIX, method_name.to_s, '#', '(', params.to_s, ')'].join
        end
      end
    end
  end
end
