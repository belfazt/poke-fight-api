# frozen_string_literal: true

require_relative 'base'

require_relative '../repository/type'

module PokeFight
  module CachedRepository
    ##
    # CachedRepository to help with moves
    class Type < Base
      CACHE_KEY_PREFIX = 'type_'

      REPOSITORY_CLASS = PokeFight::Repository::Type

      class << self
        def init
          Parallel.each(REPOSITORY_CLASS.fetch_all) { |t| find(t.name) }
        end
      end
    end
  end
end
