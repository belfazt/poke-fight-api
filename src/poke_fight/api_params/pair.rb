# frozen_string_literal: true

module PokeFight
  module APIParams
    ##
    # Pair param
    class Pair
      attr_reader :values

      def self.parse(values)
        new(values)
      end

      def initialize(csv)
        raise 'Invalid format' if csv.empty? || csv == ''

        @values = csv.split(',')

        raise 'Invalid format' if @values.size != 2
      end
    end
  end
end
