# frozen_string_literal: true

module PokeFight
  module APIParams
    ##
    # CSV param
    class CSV
      include Enumerable

      attr_reader :values

      def self.parse(values)
        new(values)
      end

      def initialize(csv)
        raise 'Invalid format' if csv.empty? || csv == ''

        @values = csv.split(',')
      end

      def each(&block)
        @values.each { |v| block.call(v) }
      end

      def size
        @values.size
      end
    end
  end
end
