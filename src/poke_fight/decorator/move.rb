# frozen_string_literal: true

require 'delegate'

module PokeFight
  module Decorator
    ##
    # Move decorator
    class Move < SimpleDelegator
      def initialize(move)
        @move = move
        super
      end

      def id
        @id ||= url.split('/').last
      end

      def name
        @move.move.name
      end

      def url
        @url ||= @move.move.url
      end
    end
  end
end
