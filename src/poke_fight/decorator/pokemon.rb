# frozen_string_literal: true

require 'delegate'

require_relative 'move'

module PokeFight
  module Decorator
    ##
    # Pokemon decorator
    class Pokemon < SimpleDelegator
      def initialize(pokemon)
        @pokemon = pokemon
        super
      end

      def moves
        @moves ||= @pokemon.moves.map { |e| Move.new(e) }
      end

      def moves_ids
        moves.map(&:id)
      end
    end
  end
end
