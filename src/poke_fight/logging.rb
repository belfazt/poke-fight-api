# frozen_string_literal: true

require 'logger'
# :nocov: #
LOG_OUTPUT_IO = if ENV['DOCKER_LOGS'] == 'true'
                  fd = IO.sysopen('/proc/1/fd/1', 'w')
                  io = IO.new(fd, 'w')
                  io.sync = true
                  io
                else
                  $stdout
                end
# :nocov: #

LOG_LEVEL = Logger.const_get(ENV.fetch('LOG_LEVEL', 'info').upcase)
LOGGER = Logger.new(LOG_OUTPUT_IO).tap { |l| l.level = LOG_LEVEL }.freeze
