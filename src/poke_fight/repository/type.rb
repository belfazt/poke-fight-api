# frozen_string_literal: true

require_relative '../connections'

module PokeFight
  module Repository
    ##
    # Repository class to fetch types
    class Type
      DEFAULT_LIMIT = 100
      DEFAULT_OFFSET = 0

      class << self
        def find(name)
          Connections.poke_api.get(type: name)
        end

        def fetch_all
          offset = DEFAULT_OFFSET
          results = []
          results << Connections.poke_api.get(type: { limit: DEFAULT_LIMIT, offset: offset })
          while results.last.next_url
            offset += DEFAULT_LIMIT
            results << Connections.poke_api.get(type: { limit: DEFAULT_LIMIT, offset: offset })
          end

          results.map(&:results).flatten
        end
      end
    end
  end
end
