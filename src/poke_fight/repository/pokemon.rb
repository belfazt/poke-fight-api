# frozen_string_literal: true

require_relative '../connections'

require_relative '../decorator/pokemon'

module PokeFight
  module Repository
    ##
    # Repository class to fetch pokemons
    class Pokemon
      class << self
        def find(id_or_name)
          Decorator::Pokemon.new(Connections.poke_api.get(pokemon: id_or_name))
        rescue JSON::ParserError => e
          LOGGER.error(e)
          raise UndiscoveredPokemon, id_or_name
        end

        def list(ids_or_names)
          ids_or_names.map { |e| find(e) }
        end
      end
    end
  end
end
