# frozen_string_literal: true

require 'iso-639'

module PokeFight
  ##
  # Utilities that I had no idea where to put, or that don't belong anywhere else... that's my justification :D
  class Util
    class << self
      def languages
        @languages ||= ISO_639::ISO_639_1.each_with_object({}) { |e, h| h[e[2]] = e[3] }
      end
    end
  end
end
