# frozen_string_literal: true

require_relative '../api_params/pair'

require_relative 'cached_use_case/battle'

require_relative 'serializer/battle'

module PokeFight
  module APIv1
    ##
    # Groups the endpoint(s) that deal with retrieving abilities of different pokemons
    class Battle < Grape::API
      version :v1, using: :path

      params do
        requires :pokemons, type: PokeFight::APIParams::Pair, documentation: {
          type: 'string',
          collectionFormat: 'multi',
          default: 'pikachu,mewtwo',
          desc: 'CSV pair of pokemon names or ids'
        }
        optional :include, type: String, values: %w[pokemons], default: nil
      end
      desc 'It returns which pokemon would best the other one in a battle' do
        failure [
          { code: 404, message: 'Pokemon not found/discovered' },
          { code: 400, message: 'Invalid params' }
        ]
      end
      get '/battles' do
        Serializer::Battle.new(
          CachedUseCase::Battle.new(*params[:pokemons].values).fetch,
          include: [params[:include]]
        )
      end
    end
  end
end
