# frozen_string_literal: true

require 'fast_jsonapi'

module PokeFight
  module APIv1
    module Serializer
      ##
      # Pokemon serializer
      class Pokemon
        include FastJsonapi::ObjectSerializer

        set_type :pokemon

        set_id :id

        attributes :base_experience,
                   :height,
                   :is_default,
                   :abilities,
                   :order,
                   :weight,
                   :forms,
                   :game_indices,
                   :held_items,
                   :location_area_encounters,
                   :species,
                   :sprites,
                   :stats,
                   :types

        has_many :moves, id_method_name: :moves_ids
      end
    end
  end
end

module PokeFight
  module APIv1
    module Serializer
      ##
      # Alias class
      class BattlePokemonSerializer < Pokemon; end
    end
  end
end
