# frozen_string_literal: true

require 'fast_jsonapi'

require_relative 'pokemon'

module PokeFight
  module APIv1
    module Serializer
      ##
      # Move serializer
      class Battle
        include FastJsonapi::ObjectSerializer

        class << self
          def generate_explanation(battle) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
            types_comp = []
            types_comp << "#{battle.first} is of types #{battle.first_types.join(', ')} " \
                          "and #{battle.second} is of types #{battle.second_types.join(', ')}."

            [
              { damage: battle.first_deals_double_damage, direction: :to, multiplier: :double },
              { damage: battle.first_receives_half_damage, direction: :from, multiplier: :half },
              { damage: battle.first_deals_double_damage, direction: :from, multiplier: :no }
            ].each do |e|
              next unless e[:damage].any?

              types_comp << if e[:direction] == :to
                              "A #{e[:damage].first} type attack can deal #{e[:multiplier]}" \
                                            " damage to a #{e[:damage].last} pokemon"
                            else
                              "A #{e[:damage].first} pokemon can receive #{e[:multiplier]}" \
                                            " damage from a #{e[:damage].last} type attack"
                            end
            end

            types_comp << 'These pokemons deal normal damage to each other' if types_comp.size == 1

            types_comp.join("\n")
          end
        end

        set_type :battle

        set_id :id

        attributes :first,
                   :second

        has_many :pokemons

        meta do |battle|
          {
            can_the_first_deal_double_damage: battle.first_deals_double_damage.any?,
            can_the_first_receive_half_damage: battle.first_receives_half_damage.any?,
            can_the_first_receive_no_damage: battle.first_receives_no_damage.any?,
            explanation: generate_explanation(battle)
          }
        end
      end
    end
  end
end
