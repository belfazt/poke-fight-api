# frozen_string_literal: true

require 'fast_jsonapi'

module PokeFight
  module APIv1
    module Serializer
      ##
      # Move serializer
      class Move
        include FastJsonapi::ObjectSerializer

        set_type :move

        set_id :id

        attributes :name, :url
      end
    end
  end
end

module PokeFight
  module APIv1
    module Serializer
      ##
      # Alias class
      class PokemonMoveSerializer < Move; end
    end
  end
end
