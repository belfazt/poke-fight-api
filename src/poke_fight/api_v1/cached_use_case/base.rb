# frozen_string_literal: true

require 'parallel'

require_relative '../../connections'

module PokeFight
  module APIv1
    module CachedUseCase
      ##
      # Base CachedUseCase class
      class Base
        def cache_key(method_name, params = '--no_params--')
          ['use_case_', cache_key_prefix, method_name.to_s, '(', params.to_s, ')'].join
        end
      end
    end
  end
end
