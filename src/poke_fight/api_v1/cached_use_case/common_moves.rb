# frozen_string_literal: true

require_relative 'base'

require_relative '../use_case/common_moves'

require_relative '../../connections'

module PokeFight
  module APIv1
    module CachedUseCase
      ##
      # Cached use case to fetch the common moves
      class CommonMoves < Base
        CACHE_KEY_PREFIX = 'common_moves_'

        def initialize(pokemons, language, limit)
          sorted_pokemons = pokemons.values.sort
          @use_case = UseCase::CommonMoves.new(sorted_pokemons, language, limit)
          @cache_key = cache_key(:fetch, { pokemons: sorted_pokemons, language: language, limit: limit })
        end

        def fetch
          found = Connections.memcached.get(@cache_key)
          return found if found

          @use_case.fetch.tap { |r| Connections.memcached.set(@cache_key, r) }
        end

        private

        def cache_key_prefix
          CACHE_KEY_PREFIX
        end
      end
    end
  end
end
