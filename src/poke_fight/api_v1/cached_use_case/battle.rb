# frozen_string_literal: true

require_relative 'base'

require_relative '../use_case/battle'

require_relative '../../connections'

module PokeFight
  module APIv1
    module CachedUseCase
      ##
      # Cached use case to fetch the common moves
      class Battle < Base
        CACHE_KEY_PREFIX = 'battle_'

        def initialize(first_pokemon, second_pokemon)
          @use_case = UseCase::Battle.new(first_pokemon, second_pokemon)
          @cache_key = cache_key(:fetch, { first_pokemon: first_pokemon, second_pokemon: second_pokemon })
        end

        def fetch
          found = Connections.memcached.get(@cache_key)
          return found if found

          @use_case.fetch.tap { |r| Connections.memcached.set(@cache_key, r) }
        end

        private

        def cache_key_prefix
          CACHE_KEY_PREFIX
        end
      end
    end
  end
end
