# frozen_string_literal: true

require 'deep_open_struct'

require_relative '../../cached_repository/pokemon'
require_relative '../../cached_repository/type'

require_relative '../../exceptions'

module PokeFight
  module APIv1
    module UseCase
      ##
      # Use case to fetch the common moves
      class Battle
        def initialize(first_pokemon, second_pokemon)
          @first_pokemon = first_pokemon.freeze
          @second_pokemon = second_pokemon.freeze
        end

        def fetch # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
          deals_dd = damage_relation(multiplier: 'double', direction: 'to')
          receives_hd = damage_relation(multiplier: 'half', direction: 'from')
          receives_nd = damage_relation(multiplier: 'no', direction: 'from')
          DeepOpenStruct.new(
            id: [pokemons.first.name, '_vs_', pokemons.last.name].join,
            first: pokemons.first.name,
            second: pokemons.last.name,
            first_deals_double_damage: deals_dd,
            first_receives_half_damage: receives_hd,
            first_receives_no_damage: receives_nd,
            pokemon_ids: pokemons.map(&:id),
            pokemons: pokemons,
            first_types: types_for(pokemon: pokemons.first),
            second_types: types_for(pokemon: pokemons.last)
          )
        end

        private

        def damage_relations
          @damage_relations ||= PokeFight::CachedRepository::Type.list(
            types_for(pokemon: pokemons.first)
          ).each_with_object({}) { |e, h| h[e.name] = e.damage_relations }
        end

        def pokemons
          @pokemons ||= PokeFight::CachedRepository::Pokemon.list([@first_pokemon, @second_pokemon])
        end

        def damage_relation(multiplier:, direction:)
          damage_relations
            .each do |k, v|
              v.send("#{multiplier}_damage_#{direction}")
               .each do |e|
                types_for(pokemon: pokemons.last)
                  .each { |t| return [k, t] if t == e.name }
              end
            end
          []
        end

        def types_for(pokemon:)
          @types_for_mem ||= {}
          @types_for_mem[pokemon] ||= pokemon.types.map { |e| e.type.name }
        end
      end
    end
  end
end
