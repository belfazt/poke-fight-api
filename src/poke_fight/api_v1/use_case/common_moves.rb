# frozen_string_literal: true

require 'deep_open_struct'

require_relative '../../cached_repository/pokemon'
require_relative '../../cached_repository/move_translation'

require_relative '../../exceptions'

module PokeFight
  module APIv1
    module UseCase
      ##
      # Use case to fetch the common moves
      class CommonMoves
        def initialize(pokemons, language, limit)
          @pokemons = pokemons.freeze
          @language = language.freeze
          @limit = limit.freeze
        end

        def fetch
          return [] if @limit.zero?

          return pokemon_objects.first.moves[0..@limit] if @pokemons.size == 1

          colliding_moves_map.each_with_object([]) do |(_, v), a|
            a << v[:move] if v[:count] == @pokemons.size
            break a unless a.size < @limit
          end
        end

        private

        def pokemon_objects
          @pokemon_objects ||= PokeFight::CachedRepository::Pokemon.list(@pokemons)
        end

        def colliding_moves_map # rubocop:disable Metrics/AbcSize
          pokemon_objects.each_with_object(
            Hash.new { |h, k| h[k] = { count: 0, move: nil } }
          ) do |p, h|
            p.moves.each do |m|
              proxy_obj = PokeFight::CachedRepository::MoveTranslation.find(m.name)
              h[m.id][:count] += 1
              h[m.id][:move] = translated_move(proxy_obj) unless h[m.id][:move]
            end
          end
        end

        def translated_move(proxy_obj)
          language_obj = proxy_obj.dig(:names, @language)

          raise UnsupportedLanguageError, @language unless language_obj

          DeepOpenStruct.new(
            id: proxy_obj.fetch(:move).id,
            url: proxy_obj.fetch(:move).url,
            name: language_obj.name
          )
        end
      end
    end
  end
end
