# frozen_string_literal: true

require_relative '../util'

require_relative '../api_params/csv'

require_relative 'cached_use_case/common_moves'

require_relative 'serializer/move'

module PokeFight
  module APIv1
    ##
    # Groups the endpoint(s) that deal with retrieving abilities of different pokemons
    class CommonMoves < Grape::API
      version :v1, using: :path

      params do
        requires :pokemons, type: PokeFight::APIParams::CSV, documentation: {
          type: 'string',
          collectionFormat: 'multi',
          default: 'charizard,bulbasaur,lugia',
          desc: 'CSV list of pokemon names or ids'
        }
        optional :iso639, type: String, regexp: /[a-z]{2}/, values: Util.languages.keys, default: 'en', documentation: {
          desc: 'Language key in the ISO 639-1 format'
        }
        optional :limit, type: Integer, values: (0..50), default: 20
      end
      desc 'Returns the common moves of several pokemons' do
        failure [
          { code: 404, message: 'Pokemon not found/discovered' },
          { code: 412, message: 'Language not supported' },
          { code: 400, message: 'Invalid params' }
        ]
      end
      get '/common_moves' do
        Serializer::Move.new(
          CachedUseCase::CommonMoves.new(
            params[:pokemons],
            params[:iso639],
            params[:limit]
          ).fetch
        ).serializable_hash
      end
    end
  end
end
