# frozen_string_literal: true

require_relative 'cached_repository/type'
require_relative 'cached_repository/move'
require_relative 'cached_repository/move_translation'

PokeFight::CachedRepository::Type.init
PokeFight::CachedRepository::Move.init
PokeFight::CachedRepository::MoveTranslation.init
