# frozen_string_literal: true

require_relative 'configuration'

require 'poke-api-v2'
require 'dalli'

module PokeFight
  ##
  # Manages connections to external services
  class Connections
    class << self
      def memcached
        return @memcached if defined?(@memcached)

        config = Configuration.memcached
        options = { compress: config.compress, namespace: config.namespace, expires_in: config.expiration_ttl }
        @memcached = Dalli::Client.new(config.uri, options)
      end

      def poke_api
        PokeApi
      end
    end
  end
end
