# frozen_string_literal: true

ENV['ENVIRONMENT'] = ENV['RACK_ENV'] ||= 'development'

require 'deep_open_struct'

require 'dotenv'

require_relative 'logging'

module PokeFight
  ##
  # Manages configuration values
  class Configuration
    Dotenv.load(*Dir['config/.env.local', "config/.env.#{ENV['ENVIRONMENT']}"])

    CONFIGURATION = DeepOpenStruct.new(
      memcached: {
        uri: ENV.fetch('MEMCACHED_URI'),
        namespace: ENV.fetch('MEMCACHED_NAMESPACE', 'pokefight'),
        compress: ['true', true].include?(ENV.fetch('MEMCACHED_COMPRESS', true)),
        expiration_ttl: Integer(ENV.fetch('MEMCACHED_EXPIRATION_TTL', 0))
      }
    ).freeze

    private_constant :CONFIGURATION

    class << self
      def method_missing(method_name, *args, &block)
        if CONFIGURATION.respond_to?(method_name)
          CONFIGURATION.public_send(method_name, *args, &block)
        else
          super
        end
      end

      def respond_to_missing?(method_name, *args)
        CONFIGURATION.respond_to?(method_name) || super
      end
    end
  end
end
