# frozen_string_literal: true

require_relative 'src/poke_fight/cached_repository'

require_relative 'src/poke_fight/api.rb'

run PokeFight::API
