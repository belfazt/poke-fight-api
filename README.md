# Poke Fight API

[![pipeline status](https://gitlab.com/belfazt/poke-fight-api/badges/master/pipeline.svg)](https://gitlab.com/belfazt/poke-fight-api/-/commits/master)[![coverage report](https://gitlab.com/belfazt/poke-fight-api/badges/master/coverage.svg)](https://gitlab.com/belfazt/poke-fight-api/-/commits/master)

This API aggregates information that's available in [https://pokeapi.co/](https://pokeapi.co/), there are only two features at the moment:

- Retrieve the moves that several pokemons have in common, this also supports localization (for the move names)
  - [/api/v1/common_moves?pokemons=1,2,3&limit=20&iso639=es](https://pokedex.latortuga.ninja/api/v1/common_moves?pokemons=1,2,3&limit=20&iso639=es)

- Check the amount of damage that a pokemon would do to another depending on their types
  - [/api/v1/battles?pokemons=pikachu,mewtwo&include=pokemons](https://pokedex.latortuga.ninja/api/v1/battles?pokemons=pikachu,mewtwo&include=pokemons)

The status can be seen [here](https://pokedex.latortuga.ninja/api/status), documentation can be found [here](https://pokedex.latortuga.ninja/api/docs/swagger.json), feel free to render it using [petstore](https://petstore.swagger.io), just place `https://pokedex.latortuga.ninja/api/docs/swagger.json` by the top

## Setting up your environment

- You'll need ruby, preferably 2.7.1, as this is the version that it was built with, I recommend using [rbenv](https://github.com/rbenv/rbenv#installation)
- You'll need [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/)

## Developing

### Getting Dependencies

```bash
docker-compose up -d # This will start a memcached server, exposing it on port 11211
gem install bundler
bundle install
```

### Linting

```bash
bundle exec rake rubocop
```

### Testing

```bash
bundle exec rake test
```

### Generating OAPI/Swagger documentation

```bash
bundle exec rake render_swagger 1> swagger.json
```

### Running a dockerized version of the API

```bash
docker-compose -f application.yml up
```

## Deploying

```bash
kubectl create namespace pokemon # so that we don't use the default namespace
kubectl apply -f api-service.yaml,memcached-deployment.yaml,memcached-replica-deployment.yaml,memcached-replica-service.yaml,memcached-service.yaml,api-deployment.yaml -n pokemon
```

### Exposing the API service

```bash
kubectl expose --namespace=pokemon deployment api --type=LoadBalancer --port=3000 --target-port=3000 --name=poke-elb
kubectl --namespace=pokemon describe service poke-elb | grep Ingress # to get the external address of your service
```

## Architecture

The architecture is a bit funny... at the best, there were several compromises that I made, there's a [decision log](docs/DECISION_LOG.md) to keep them on track.

Here's a diagram explaining the internal layers of the API

![](docs/component_diagram.png)

## TODO

- [x] Add endpoint to get pokemons to _battle_
- [x] Add endpoint to fetch the moves that different pokemons share
- [x] Continuous Integration, there's a [GitLab CI](.gitlab-ci.yml) that takes care of that
- [ ] Continuous Delivery
  - [x] Images are built and pushed to a registry during the CI process
  - [ ] The kubernetes cluster gets changes applied in the midst of a pipeline after all of the checks have gone under
    - [ ] kubectl in the CI has to be configured properly, everything else seems to be alright

## Contributors
* Diego Camargo
