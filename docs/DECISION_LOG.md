# Decision Log

## Why memcached?

I thought about using a database to enable it as a cache, however, managing it would've taken time, and it would've also required that extra logic was added to handle data that wasn't in the database, of course, this could've been in the same manner that it was done with memcache.

The original idea was to have a datastore and background jobs to refresh the data every X amount of time, so that we'd receive updates, but, this would've been more work, and also, it would've been a bit funny to have information being copied from one place to another, when the information is readily available elsewhere. Memcached enables the API to have the flexibility at a code level, and requires almost no setup, which for the duration of this project, seemed ideal. Plus, the real idea behind the caching would be to have requests to go a bit faster, not to have the data store.

The timeline was a bit tight, as not only the code had to be done in 2 days, but also the infrastructure, and a maintanable way to replicate it in other environments.

## Why Ruby?

I've written a lot of ruby in the past, I like its syntax and how easy it is, in general. It is easy to write and easy to read too.
