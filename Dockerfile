FROM ruby:2.7.1-alpine

ADD Gemfile /app/
ADD Gemfile.lock /app/

RUN apk --update add --virtual build-dependencies ruby-dev build-base && \
    gem install bundler && \
    cd /app ; bundle config set without 'development test'; bundle install && \
    apk del build-dependencies

ADD config.ru /app
ADD Procfile /app
ADD src/ /app/src
RUN chown -R nobody:nobody /app
USER nobody
WORKDIR /app
ENV PORT=3000
ENV HOST=0.0.0.0
ENV DOCKER_LOGS=true
CMD bundle exec foreman start
EXPOSE 3000
