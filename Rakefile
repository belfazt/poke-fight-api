# frozen_string_literal: true

task :environment do
  ENV['ENVIRONMENT']
end

build_tasks = %i[environment]

begin
  require 'rake/testtask'

  Rake::TestTask.new do |t|
    t.libs << 'test'
    t.test_files = FileList['test/**/test*.rb']
    t.verbose = true
    t.warning = false
  end

  require 'bundler/audit/task'
  Bundler::Audit::Task.new

  require 'rubocop/rake_task'
  RuboCop::RakeTask.new

  build_tasks = %i[render_swagger test bundle:audit rubocop]
rescue LoadError
  raise if %w[development test ci].include?(ENV['ENVIRONMENT'])
end

task :render_swagger do
  require_relative 'src/poke_fight/api'

  require 'grape-swagger/rake/oapi_tasks'
  GrapeSwagger::Rake::OapiTasks.new(::PokeFight::API)

  Rake::Task['oapi:fetch'].execute
end

task build: build_tasks

task default: :build
