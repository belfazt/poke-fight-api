# frozen_string_literal: true

require_relative '../test_helper'

require './src/poke_fight/cached_repository/move_translation'

class TestMoveTranslationCachedRepository < Minitest::Test
  def setup
    VCR.use_cassette('TestMoveTranslationCachedRepository#setup', record: :new_episodes) do
      PokeFight::CachedRepository::MoveTranslation.init
    end
  end

  def test_find
    PokeFight::Connections.poke_api.expects(:get).never
    move = PokeFight::CachedRepository::MoveTranslation.find('pound')
    assert_equal(move[:move].id, 1)
    assert_equal(move.dig(:names, 'de').name, 'Pfund')
  end
end
