# frozen_string_literal: true

require_relative '../test_helper'

require './src/poke_fight/cached_repository/move'

class TestMoveCachedRepository < Minitest::Test
  def setup
    VCR.use_cassette('TestMoveCachedRepository#setup', record: :new_episodes) do
      PokeFight::CachedRepository::Move.init
    end
  end

  def test_find
    PokeFight::Connections.poke_api.expects(:get).never
    move = PokeFight::CachedRepository::Move.find('pound')
    assert_equal('pound', move.name)
  end

  def test_fetch_all
    PokeFight::Connections.poke_api.expects(:get).never
    moves = PokeFight::CachedRepository::Move.fetch_all
    assert_equal(746, moves.size)
  end
end
