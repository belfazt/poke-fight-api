# frozen_string_literal: true

require_relative '../test_helper'

require './src/poke_fight/repository/pokemon'

class TestPokemonRepository < Minitest::Test
  def test_find
    VCR.use_cassette('TestPokemonRepository#test_find') do
      pokemon = PokeFight::Repository::Pokemon.find(1)
      assert_equal('bulbasaur', pokemon.name)
    end
  end

  def test_list
    VCR.use_cassette('TestPokemonRepository#test_list') do
      pokemons = PokeFight::Repository::Pokemon.list([1, 2])
      assert_equal('bulbasaur', pokemons.first.name)
      assert_equal('ivysaur', pokemons.last.name)
    end
  end
end
