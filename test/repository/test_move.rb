# frozen_string_literal: true

require_relative '../test_helper'

require './src/poke_fight/repository/move'

class TestMoveRepository < Minitest::Test
  def test_find
    VCR.use_cassette('TestMoveRepository#test_find') do
      move = PokeFight::Repository::Move.find(1)
      assert_equal('pound', move.name)
    end
  end

  def test_fetch_all
    VCR.use_cassette('TestMoveRepository#fetch_all') do
      moves = PokeFight::Repository::Move.fetch_all
      assert_equal(746, moves.size)
      assert_equal('pound', moves.first.name)
    end
  end
end
