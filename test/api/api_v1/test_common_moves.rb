# frozen_string_literal: true

require_relative '../../test_helper'

class TestCommonMoves < TestApp
  def test_that_common_moves_fails_without_params
    get('/api/v1/common_moves')
    refute(last_response.ok?)
    Approvals.verify(
      last_response.body,
      format: :json,
      name: 'TestCommonMoves#test_that_common_moves_fails_without_params'
    )
  end

  def test_common_moves
    VCR.use_cassette('TestCommonMoves#test_common_moves') do
      get('/api/v1/common_moves?pokemons=1,2,3,4')
      assert(last_response.ok?)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestCommonMoves#test_common_moves'
      )
    end
  end

  def test_common_moves_but_in_german
    VCR.use_cassette('TestCommonMoves#test_common_moves_but_in_german') do
      get('/api/v1/common_moves?pokemons=1,2,3,4&iso639=de')
      assert(last_response.ok?)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestCommonMoves#test_common_moves_but_in_german'
      )
    end
  end

  def test_unsupported_language
    VCR.use_cassette('TestCommonMoves#test_unsupported_language') do
      get('/api/v1/common_moves?pokemons=1,2,3,4&iso639=ru')
      refute(last_response.ok?)
      assert_equal(412, last_response.status)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestCommonMoves#test_unsupported_language'
      )
    end
  end

  def test_out_of_range_limit
    VCR.use_cassette('TestCommonMoves#test_out_of_range_limit') do
      get('/api/v1/common_moves?pokemons=1,2,3,4&limit=51')
      refute(last_response.ok?)
      assert_equal(400, last_response.status)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestCommonMoves#test_out_of_range_limit'
      )
    end
  end

  def test_invalid_language
    VCR.use_cassette('TestCommonMoves#test_invalid_language') do
      get('/api/v1/common_moves?pokemons=1,2,3,4&iso639=ph')
      refute(last_response.ok?)
      assert_equal(400, last_response.status)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestCommonMoves#test_invalid_language'
      )
    end
  end

  def test_undiscovered_pokemon
    VCR.use_cassette('TestCommonMoves#test_undiscovered_pokemon') do
      get('/api/v1/common_moves?pokemons=ljkhsbfhabsdfhjbashjdfbahsjdf')
      refute(last_response.ok?)
      assert_equal(404, last_response.status)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestCommonMoves#test_undiscovered_pokemon'
      )
    end
  end
end
