# frozen_string_literal: true

require_relative '../../test_helper'

class TestBattle < TestApp
  def test_that_battle_fails_without_params
    get('/api/v1/battles')
    refute(last_response.ok?)
    Approvals.verify(
      last_response.body,
      format: :json,
      name: 'TestBattle#test_that_battle_fails_without_params'
    )
  end

  def test_battle
    VCR.use_cassette('TestBattle#test_battle') do
      get('/api/v1/battles?pokemons=1,2')
      assert(last_response.ok?)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestBattle#test_battle'
      )
    end
  end

  def test_battle_with_includes
    VCR.use_cassette('TestBattle#test_battle_with_includes') do
      get('/api/v1/battles?pokemons=1,2&include=pokemons')
      assert(last_response.ok?)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestBattle#test_battle_with_includes'
      )
    end
  end

  def test_battle_pikachu_vs_mewtwo
    VCR.use_cassette('TestBattle#test_battle_pikachu_vs_mewtwo') do
      get('/api/v1/battles?pokemons=pikachu,mewtwo&include=pokemons')
      assert(last_response.ok?)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestBattle#test_battle_pikachu_vs_mewtwo'
      )
    end
  end

  def test_battle_pikachu_vs_missingno
    VCR.use_cassette('TestBattle#test_battle_pikachu_vs_missingno') do
      get('/api/v1/battles?pokemons=pikachu,missingno&include=pokemons')
      refute(last_response.ok?)
      Approvals.verify(
        last_response.body,
        format: :json,
        name: 'TestBattle#test_battle_pikachu_vs_missingno'
      )
    end
  end
end
