# frozen_string_literal: true

require 'simplecov'

SimpleCov.start do
  add_filter('test')
  add_filter('vendor')
end

ENV['ENVIRONMENT'] = ENV['RACK_ENV'] ||= 'test'

require 'pry'
require 'minitest/autorun'
require 'minitest/reporters'
require 'mocha/minitest'
require 'rack/test'

Minitest::Reporters.use!

OUTER_APP = Rack::Builder.parse_file('config.ru').first

require 'approvals'

Approvals.configure do |c|
  c.approvals_path = 'test/fixtures/approvals/'
end

require 'webmock/minitest'
require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = 'test/fixtures/vcr_cassettes'
  config.hook_into :webmock
end

class TestApp < Minitest::Test
  include Rack::Test::Methods

  def app
    OUTER_APP
  end
end
